using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento_personaje : MonoBehaviour
{

    public Animator personajeAnimator;

    public SpriteRenderer personajeSprite;

    
    


    public float vel = 0.01f;

    private void Start()
    {
        
       
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {

            personajeAnimator.SetBool("Run", true);
            transform.position += Vector3.left * vel;
            personajeSprite.flipX = true;

        }

        else if (Input.GetKey(KeyCode.D)){

            
            personajeAnimator.SetBool("Run", true);
            transform.position += Vector3.right * vel;

            personajeSprite.flipX = false;
            

        }

        else
        {

            personajeAnimator.SetBool("Run", false);

        }

        if (Input.GetKey(KeyCode.Space))
        {
            personajeAnimator.SetBool("Jumpup", true);
            transform.position += Vector3.up * vel;
        }

        

        



    }
}
