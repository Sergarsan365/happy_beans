using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EscenaJuego()
    {
        SceneManager.LoadScene("Juego");
    }
    public void EscenaJuego2()
    {
        SceneManager.LoadScene("Juego2");
    }
    public void ResetJuego()
    {
        SceneManager.LoadScene("Juego");
        PlayerPrefs.DeleteAll();
    }
    public void Salir()
    {
        Application.Quit();
//#if UNITY_EDITOR
//#else
        //Application.Quit();
//#endif
    }
    public void menu()
    {
        SceneManager.LoadScene("Menu Inicial");
    }
}
