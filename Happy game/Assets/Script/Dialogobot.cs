using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogobot : MonoBehaviour
{
    public bool dialogo;
    public GameObject Panel;
    public bool empdialogo;
    public TMP_Text Dialogoem;
    private int LineIndex;
    private float tipeintime = 0.05f;
    [SerializeField, TextArea(2, 8)] private string[] Lineas;
    // Start is called before the first frame update
    void Start()
    {

    }
    void Update()
    {
        if (dialogo == true)
        {
        if (Input.GetKeyDown(KeyCode.E))
            {
                //(empdialogo == falso)
                if (!empdialogo)
                {
                    Dialogocomienza();
                }
            }
        }
        if (Input.GetKey(KeyCode.Q))
        {
            empdialogo = false;
            Panel.SetActive(false);
            Time.timeScale = 1f;
            Panel.SetActive(false);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("No");
        if (collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            Debug.Log("Si");
            //(empdialogo == falso)
            dialogo = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            dialogo = false;
        }
    }

    private void Dialogocomienza()
    {
        empdialogo = true;
        Panel.SetActive(true);
        LineIndex = 0;
        Time.timeScale = 0f;
        StartCoroutine(ShowLine());

    }
    private void NextDialogueLine()
    {
        LineIndex++;
        if (LineIndex < Lineas.Length)
        {
            StartCoroutine(ShowLine());
        }
        else
        {
            empdialogo = false;
            Panel.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    private IEnumerator ShowLine()
    {

        Dialogoem.text = string.Empty;
        foreach (char ch in Lineas[LineIndex])
        {
            Dialogoem.text += ch;
            yield return new WaitForSecondsRealtime(tipeintime);

        }
    }
}