using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class luzMov : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Enemy"))
        {
            collision.GetComponentInParent<mov_ara�a>().luz = true;
        }
        if (collision.GetComponent<Collider2D>().CompareTag("Enemy2"))
        {
            collision.GetComponentInParent<mov_murc>().luz = true;
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<Collider2D>().CompareTag("Enemy"))
        {
            other.GetComponentInParent<mov_ara�a>().luz = false;
        }
        if (other.GetComponent<Collider2D>().CompareTag("Enemy2"))
        {
            other.GetComponentInParent<mov_murc>().luz = false;
        }

    }
}
