using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class llave2 : MonoBehaviour
{
    public Animator Puerta;
    // Start is called before the first frame update
    void Start()
    {
        Puerta.enabled = false;
        if (PlayerPrefs.GetInt("llave", 0) == 2)
        {
            Destroy(gameObject);
            Puerta.enabled = true;
        }


    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Collider2D>().CompareTag("Player"))
        {
            Puerta.enabled = true;
            Destroy(gameObject);
            PlayerPrefs.SetInt("llave", 2);

        }
    }
}
