using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mov_murc : MonoBehaviour
{
    public float vel;
    public Transform player;
    public Rigidbody2D rb2D;
    public bool luz = false;
    private Vector2 randomDir;
    public Transform arriba;
    public float velmov;

    // Start is called before the first frame update


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            if (luz == false)
            {
                vel = velmov;
                Vector2 objetivo = new Vector2(player.position.x, player.position.y);
                Vector2 nuevaPos = Vector2.MoveTowards(rb2D.position, objetivo, vel * Time.deltaTime);
                rb2D.MovePosition(nuevaPos);
            }

        }
    }
}
