using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botoncaja : MonoBehaviour
{
    public Animator Puerta;
    public string caja;
    //public de;
    // Start is called before the first frame update
    void Start()
    {
        Puerta.enabled = false;
       
        if (PlayerPrefs.GetInt("llave", 0) == 1)
        {

            Puerta.enabled = true;
        }


    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag(caja))
        {
            Puerta.enabled = true;

        }
    }
}
